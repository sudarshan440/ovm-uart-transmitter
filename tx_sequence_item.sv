class tx_sequence_item extends ovm_sequence_item;
// variable declaration 
rand bit [7:0] tx_data;
//bit txd;
// registering with factory
`ovm_object_utils_begin(tx_sequence_item)
`ovm_field_int(tx_data,OVM_ALL_ON);
//`ovm_field_int(txd,OVM_ALL_ON);
`ovm_object_utils_end
//constructor
function new(string name = " " );
super.new(name);
endfunction: new

//declaration of constraints

endclass : tx_sequence_item