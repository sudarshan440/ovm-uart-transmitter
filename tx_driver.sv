class tx_driver extends ovm_driver#(tx_sequence_item);
tx_sequence_item seq_item;
virtual tx_intf intf;
Wrapper wrapper;
ovm_object dummy;

//registering
`ovm_component_utils(tx_driver)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

//run phase
virtual task run ();
reset_dut();
forever 
begin
seq_item_port.get_next_item(seq_item);

@(posedge intf.tx_clk)
if (intf.tx_rst == 1) begin 
intf.transmit = 1'b0; end
else 
begin 
intf.transmit = 1'b1; 
end

@(posedge intf.tx_clk)
intf.tx_data = seq_item.tx_data;
$display("data at driveris %d",seq_item.tx_data);
seq_item_port.item_done;

#100ns
intf.transmit = 1'b0; 


end //forever loop
endtask : run


task reset_dut();
       intf.tx_rst = 1;
	   intf.transmit = 0;
       #50ns ;
       intf.tx_rst = 0;

endtask
   
endclass : tx_driver 