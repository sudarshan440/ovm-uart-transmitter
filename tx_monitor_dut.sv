class tx_monitor_dut extends ovm_monitor;
tx_sequence_item seq_item;
virtual tx_intf intf;
Wrapper wrapper;
ovm_object dummy;
int state=0;
int counter;
reg [7:0] tempdata = 8'b00000000; 

ovm_analysis_port #(tx_sequence_item) mondut2sb;
//registering
`ovm_component_utils(tx_monitor_dut)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
mondut2sb = new("mondut2sb",this);
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

virtual task run();
seq_item = tx_sequence_item::type_id::create("seq_item");

forever
begin

case (state)
  0: begin
      #80ns
       if (intf.txd == 0)
         begin 
 
         state  = 1;
	     $display("txd zer detected");
        end

  
     end
  1: 
     begin
	          if (counter < 8)
               begin
					#80ns;

					$display("in tx monitor dut data is %d",intf.txd  );  

					tempdata  = {intf.txd,tempdata[7:1]};
				//  $display("tempdata is %d",tempdata);			
					counter = counter + 1;
					$display("counter is %d", counter);
					end
			else
			begin 
					counter = 0; 
					$display("tempdata is %d",tempdata);			
					state = 2; 
			end
	
	 end
	 
	 2:
	 
	 begin
	     $display("state is %d",state);
			seq_item.tx_data = tempdata   ;
		$display("seq_item.tx_data is %d",seq_item.tx_data);
		mondut2sb.write(seq_item); 
		state = 0;

	 end
     
endcase 
end

endtask : run
 
/* 
virtual task run();
seq_item = tx_sequence_item::type_id::create("seq_item");

forever
begin
state=  0;
 if (state == 0) 
   begin  
   //  @(posedge intf.tx_clk) 
      #80ns
   if (intf.txd == 0)
     begin 
 
      state  = 1;
	  $display("txd zer detected");
     end

  end
	
//@(posedge intf.tx_clk)

 
if (state == 1)
    begin
	// @(posedge intf.tx_clk)
 	        if (counter < 8)
       begin
	      #80ns;

			$display("in tx monitor dut data is %d",intf.txd  );  

			tempdata  = {intf.txd,tempdata[7:1]};
    //  $display("tempdata is %d",tempdata);			
			counter = counter + 1;
			$display("counter is %d", counter);
					end
		else
			begin 
			counter = 0; 
			$display("tempdata is %d",tempdata);			
			state = 2; 
			end
	end //upper if loop
 
if (state == 2)
begin
    #80ns
     // @(posedge intf.tx_clk)
  if (intf.txd == 1)
    state = 3;
end

if (state  ==  3)
  begin
    $display("state is %d",state);
seq_item.tx_data = tempdata   ;
$display("seq_item.tx_data is %d",seq_item.tx_data);
mondut2sb.write(seq_item); 
state = 0;
end

end

endtask : run

*/
endclass : tx_monitor_dut