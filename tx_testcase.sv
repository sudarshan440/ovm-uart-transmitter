class tx_testcase extends ovm_test;
tx_sequence seq;
tx_sequencer sequencer;
tx_environment env;
//registering
`ovm_component_utils(tx_testcase)

//constructor
function new(string name = " ", ovm_component parent= null);
super.new(name,parent);

endfunction : new

function void build();
  super.build();
  env = tx_environment::type_id::create("env",this);
endfunction : build


task run();
$cast(sequencer,env.agent.sequencer);
sequencer.count = 0;
 seq = tx_sequence::type_id::create("seq"); 
seq.start(sequencer);
endtask 

endclass : tx_testcase