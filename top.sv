module top;
    import txPkg::*;
  import txTestPkg::*;
tx_intf intf();

initial
begin
intf.tx_clk = 0;
end

always #10 intf.tx_clk = ~intf.tx_clk;
//module transmitter(clk, reset, transmit, data, TxD );
transmitter dut (.clk(intf.tx_clk),
                 .reset(intf.tx_rst),
                 .transmit(intf.transmit),
                 .data(intf.tx_data),
                 .TxD(intf.txd)				 
);

initial
begin
Wrapper wrapper = new("Wrapper");
 wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("tx_testcase");
#400;
$finish;
end


endmodule : top