class tx_sequence extends ovm_sequence#(tx_sequence_item);
tx_sequence_item seq_item;

//registering 
`ovm_object_utils(tx_sequence)

//constructor

function new(string name = " ");
super.new(name);
endfunction 

// body

virtual task body();
seq_item = tx_sequence_item::type_id::create("seq_item");
wait_for_grant();
//seq_item.randomize() with {tx_data == 8'b01010101;};
assert(seq_item.randomize());
send_request(seq_item);
wait_for_item_done();
endtask : body
endclass : tx_sequence