// Code your testbench here
// or browse Examples
module tb;

reg clk, reset, transmit;
reg [7:0] data;

wire TxD;

transmitter uut (
  .clk(clk),
.reset (reset),
.transmit(transmit),
.data(data),
.TxD(TxD)
);


initial 
begin
 // $dumpfile("dump.vcd");
 // $dumpvars(0,uart_tx_test);

 clk = 0;
reset = 1;
transmit = 0;
data = 0;

#100;


end //initial

initial
begin
  #5 reset = 0;
#10 data = 8'b01001101;
#15 transmit = 1;

end 
  
always #20 clk = ~clk;

endmodule 