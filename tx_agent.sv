class tx_agent extends ovm_agent;
tx_monitor_tb  monitortb;
tx_monitor_dut  monitordut;
tx_driver driver;
tx_sequencer sequencer;

ovm_analysis_port #(tx_sequence_item) agenttb;
ovm_analysis_port #(tx_sequence_item) agentdut;

//registering
`ovm_component_utils(tx_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

function void build();
super.build();
agenttb = new("agenttb",this);
agentdut = new("agentdut",this);

sequencer = tx_sequencer::type_id::create("sequencer",this);
driver = tx_driver::type_id::create("driver",this);
monitortb = tx_monitor_tb::type_id::create("monitortb",this);
monitordut = tx_monitor_dut::type_id::create("monitordut",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(sequencer.seq_item_export);
monitortb.montb2sb.connect(agenttb);
monitordut.mondut2sb.connect(agentdut);

endfunction : connect


endclass : tx_agent