class tx_monitor_tb extends ovm_monitor;
tx_sequence_item seq_item;
virtual tx_intf intf;
Wrapper wrapper;
ovm_object dummy;

ovm_analysis_port #(tx_sequence_item) montb2sb;
//registering
`ovm_component_utils(tx_monitor_tb)
//coverage collector
//covergroup cg;
//???????????????
//endgroup : cg 


//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
//cg = new;
endfunction: new


virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 

if($cast(wrapper,dummy))
  begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
montb2sb = new("montb2sb",this);
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

virtual task run();
seq_item = tx_sequence_item::type_id::create("seq_item");
forever
begin
  @(posedge intf.tx_clk) 

     //if ( intf.transmit == 1'b1)
	 if ( intf.tx_data != 0   )
       begin

         seq_item.tx_data = intf.tx_data;
      //   $display("in tx monitor tb data is %d",intf.tx_data );  
        //cg_inst.sample();
         //model(); 
         montb2sb.write(seq_item);
       end

end //forever loop

endtask : run

function void model();
$display("dummy model ");
endfunction : model
endclass : tx_monitor_tb