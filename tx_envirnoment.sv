class tx_environment extends ovm_env;
tx_agent agent;
scoreboard sb;
//registering
`ovm_component_utils(tx_environment)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

function void build();
super.build();
agent = tx_agent::type_id::create("agent",this);
sb = scoreboard::type_id::create("sb",this);
endfunction : build

function void connect();
agent.agenttb.connect(sb.sb_tb);
agent.agentdut.connect(sb.sb_dut);
endfunction : connect 

endclass : tx_environment